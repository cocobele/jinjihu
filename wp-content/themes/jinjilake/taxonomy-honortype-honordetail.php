<?php
/**
 * TEMPLATE NAME:俱乐部荣誉
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=2" class="u1">
                俱乐部简介
            </a>
            <a href="?page_id=33" class="u2">
            	MBI
            </a>
            <a href="?page_id=31" class="u3">
            	IMG
            </a>
            <a href="?honortype=honordetail" class="cur u4">
            	俱乐部荣誉
            </a>
            <a href="?namelisttype=namelistdetail" class="u5">
            	会员一杆进洞扬名榜
            </a>
            <a href="?champion=champion" class="u6">
            	俱乐部冠军榜
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle alignCenter" style="margin-top:14px">
                	<ul>俱乐部荣誉</ul>
                    <ul id="yearList" class="mCustomScrollbar" data-mcs-axis="x">
                       <div id="yearListDiv">
                       		
                       </div>
                    </ul>
                </div>
                <div class="contLine" style="margin-top:80px"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar" data-mcs-axis="y">
                    	<div class="honorTable">
                    		<ul>
                            	<li class="yearHonor" style="background-color:#90844a;width:100%; text-align:center">
                              		  	
                                </li>
                            </ul>
                            <ul>
                            	<li style="width:49%">
                                	获得荣誉
                                </li>
                                <li style="width:50%">
                                	颁奖单位
                                </li>
                            </ul>
                        <script language="javascript">
							function sortNumber(b, a)
							{
							return a - b
							}
							var yearArray = new Array();
							function addYear(y){
								var b = true;
								for(var i=0;i<yearArray.length;i++){
									if(y==yearArray[i]){
										b = false;
										break;
									}
								}
								if(b){
									yearArray.push(y);
								}
							}
							
						</script>
                        <?php while (have_posts()):the_post();?>
                            <ul class="<?php echo get_post_meta(get_the_ID(),'year',true)?> yearPicDetail">
                            	<li style="width:49%">
                                	<?php echo get_post_meta(get_the_ID(),'honorname',true)?>
                                </li>
                                <li style="width:50%">
                                	<?php echo get_post_meta(get_the_ID(),'unit',true)?>
                                </li>
                            </ul>
                            <script language="javascript">
								addYear(<?php echo get_post_meta(get_the_ID(),'year',true)?>);
							</script>
                        <?php endwhile;?>
                        	
                        </div>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/honor.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
			$('.fancybox').fancybox();
			
			yearArray.sort(sortNumber);
			var yldw = yearArray.length * 47;
			$("#yearListDiv").css('width',yldw);
			for(var i=0;i<yearArray.length;i++){
				var myli = "<li>"+yearArray[i]+"</li>";
				$("#yearListDiv").append(myli);
			}
			
			$('#yearListDiv li').mousedown(function(){
				$("#yearListDiv li").each(function(){
					$(this).css('background','none').css('color','#e3c166');
				});
				$(this).css('background-color','#fcebc5').css('color','#352602');
				
				$(".yearPicDetail").each(function(){
					$(this).css('display','none');
				});
				var showli = "." + $(this).text();
				$(showli).each(function(){
					$(this).css('display','block');
				});
				var curYear = $(this).text();
				$(".yearHonor").text(curYear + "年俱乐部荣誉");
			});
			
			$('#yearListDiv li:first-child').mousedown();
			
		});
		</script>
       <?php get_footer(); ?>

