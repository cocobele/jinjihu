<?php
/**
 * TEMPLATE NAME:教练简介
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=13" class="u1">
                学院简介
            </a>
            <a href="?coachtype=golfcoach" class="cur u2">
            	教练简介
            </a>
            <a href="?course=course" class="u3">
            	课程简介
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">高尔夫教练简介</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar tempPic">
                    	<?php while (have_posts()):the_post();?>
                    	<div class="coachLeft">
                        	<img src="<?php echo get_field('coachthumb') ?>" />
                            <p>驻场教练 <?php the_title(); ?></p>
                        </div>
                        <div class="coachRight">
                        	<?php the_content(); ?>
                        </div>
                        
                         <script type=""text/javascript"">
						$(document).ready(function(){
						  $('BODY').bgStretcher({
							images: ['<?php echo get_field('bkpic') ?>'],
							imageWidth: 1920,
							imageHeight: 900,
							slideDirection: 'N',
							nextSlideDelay:6000,
							slideShowSpeed: 1000,
							transitionEffect: 'fade',
							sequenceMode: 'normal',
							buttonPrev: '#prev',
							buttonNext: '#next',
							pagination: '#nav',
							anchoring: 'left center',
							anchoringImg: 'left center'
							});
							
						});
						</script>
                        <?php endwhile;?>
                        <div class="page_navi"><?php par_pagenavi(9); ?></div>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
       
        
<?php get_footer(); ?>