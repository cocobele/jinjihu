<?php
/**
 * TEMPLATE NAME:雪茄吧
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=15" class="u1">
                专卖店
            </a>
            <a href="?page_id=50" class="cur u2">
            	Gary Player 雪茄吧
            </a>
            <a href="?page_id=52" class="u3">
                马可波罗西餐厅
            </a>
            <a href="?page_id=54" class="u4">
            	湖轩宴会厅
            </a>
            <a href="?page_id=56" class="u5">
                贵宾专属区
            </a>
            <a href="?page_id=58" class="u6">
            	会议室
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">Gary player 雪茄吧</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<p>
                        	<div class="threePic">
                            	<ul>
                                	<li>
                                    	<img src="<?php echo get_template_directory_uri()?>/img/cigar1.jpg" />
                                    </li>
                                    <li>
                                    	<img src="<?php echo get_template_directory_uri()?>/img/cigar2.jpg" />
                                    </li>
                                    <li>
                                    	<img src="<?php echo get_template_directory_uri()?>/img/cigar3.jpg" />
                                    </li>
                                </ul>
                                <div class="clearFloat"></div>
                            </div>
                            
                        </p>
                    	<p>红酒雪茄吧以球场设计师"黑骑士"Gary Player命名，典藏了Gary Player的诸多荣誉象征。您可于打球之余，与二三知己球友，来此放松休闲，或分享打球心得，或洽谈事务，静享属于您的悠然时光。私密而安静的空间，奢华而典雅的陈设、优质而贴
心的服务，俱乐部用心为您打造高品质的品味享受。</p>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/cigarBar.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>