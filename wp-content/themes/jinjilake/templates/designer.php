<?php
/**
 * TEMPLATE NAME:设计师
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=11" class="u1">
                球场简介
            </a>
            <a href="?page_id=41" class="u2">
            	GEO
            </a>
            <a href="?page_id=9" class="cur u3">
            	设计师
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">设计师</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
						<p>Gary Player 中国华东地区首个签名设计球场。</p>
                    	<p>从球场到设计图纸，世界高尔夫三巨头之一、"黑骑士"Gary Player一直创造着属于他的奇迹，用永不妥协的完美主义理念演绎经典。大师跨越半个地球，不远万里的行程，只为每个细节都无愧于球场设计图纸上郑重落下的签名</p>
						<p>征战世界各地无数经典球场的回忆，清晰的体现在Gary Player 思考过的每个细节中。球道的挑战性及整体攻略、每个沙坑的布局及形状、树木排列和品种、甚至球场每处草坪的起伏……完美主义的设计要求，在苏州金鸡湖高尔夫得到完美的演绎。</p>
                        <br/>
                        <p align="right"><img src="" class="signatureImg" style="display:none" /></p>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/designer.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
			setTimeout(function(){
				$('.signatureImg').css('display','block').attr("src","<?php echo get_template_directory_uri()?>/img/signature.gif");
			},3000);
			
			setTimeout(function(){
				$('.signatureImg').attr("src","<?php echo get_template_directory_uri()?>/img/signature.png");
			},5000);
			
		});
		</script>
<?php get_footer(); ?>