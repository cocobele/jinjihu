<?php
/**
 * TEMPLATE NAME:电子会刊
 */
get_header();
?>       
        
        <div class="LnavBg">
        	<a href="?cat=3" class="u1">
                俱乐部动态
            </a>
            <a href="?page_id=60" class="cur u2">
            	电子会刊
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">电子会刊</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<ul class="threePart">
                        	<li class="alignCenter">
                            	<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo get_template_directory_uri()?>/img/eJournal1.jpg">
                            		<img src="<?php echo get_template_directory_uri()?>/img/eJournal1.jpg" />
                                </a>
                                第一期
                            </li>
                            <li class="alignCenter">
                            	<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo get_template_directory_uri()?>/img/eJournal2.jpg">
                            		<img src="<?php echo get_template_directory_uri()?>/img/eJournal2.jpg" />
                                </a>
                                第二期
                            </li>
                            <li class="alignCenter">
                            	<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo get_template_directory_uri()?>/img/eJournal3.jpg">
                            		<img src="<?php echo get_template_directory_uri()?>/img/eJournal3.jpg" />
                                </a>
                                第三期
                            </li><div class="clearFloat"></div>
                        </ul>
                        <ul class="threePart">
                        	<li class="alignCenter">
                            	<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo get_template_directory_uri()?>/img/eJournal4.jpg">
                            		<img src="<?php echo get_template_directory_uri()?>/img/eJournal4.jpg" />
                                </a>
                                第四期
                            </li>
                            <li class="alignCenter">
                            	<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo get_template_directory_uri()?>/img/eJournal5.jpg">
                            		<img src="<?php echo get_template_directory_uri()?>/img/eJournal5.jpg" />
                                </a>
                                第五期
                            </li>
                            <li class="alignCenter">
                            	<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo get_template_directory_uri()?>/img/eJournal6.jpg">
                            		<img src="<?php echo get_template_directory_uri()?>/img/eJournal6.jpg" />
                                </a>
                                第六期
                            </li><div class="clearFloat"></div>
                        </ul>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/eJournal.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
					$('.fancybox-buttons').fancybox({
						padding:0,
						margin:0,
						autoSize:true,
						openEffect  : 'elastic',
						closeEffect : 'elastic',
		
						prevEffect : 'fade',
						nextEffect : 'fade',
		
						closeBtn  : true,
		
						helpers : {
							title : {
								type : 'inside'
							},
							buttons	: {}
						},
		
						
					});
			
		});
		</script>

<?php get_footer(); ?>