<?php
/**
 * TEMPLATE NAME:会议室
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=15" class="u1">
                专卖店
            </a>
            <a href="?page_id=50" class="u2">
            	Gary Player 雪茄吧
            </a>
            <a href="?page_id=52" class="u3">
                马可波罗西餐厅
            </a>
            <a href="?page_id=54" class="u4">
            	湖轩宴会厅
            </a>
            <a href="?page_id=56" class="u5">
                贵宾专属区
            </a>
            <a href="?page_id=58" class="cur u6">
            	会议室
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">会议室</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<p>位于三楼的会议室拥有绝对的私密性，环境优雅，宽敞明亮，整个会议室线条明晰，奢华却不张扬，沉稳却不古板。更配有高级的投影设备，是您举行公司会议的不二选择。</p>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/conferenceRoom.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>