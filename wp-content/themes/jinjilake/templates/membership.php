<?php
/**
 * TEMPLATE NAME:会籍
 */
get_header();
?>        
        <div class="LnavBg">
        	<a href="membership.html" class="cur u1">
                会籍
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">加入 "金鸡湖高尔夫" ！</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<p>苏州金鸡湖国际高尔夫俱乐部是国内独具特色的纯会员制俱乐部，所有新会员均由老会员推荐其朋友，家人，生意伙伴，从而得以加入。国际化的大家庭吸引着来自10个不同国家的成员形成了多样化的文化氛围和极具活力的会员环境。"金鸡湖高
尔夫"的独特性更彰显在对文化差异孜孜不倦的追求和利用以高尔夫赛事为首的多种方式不断促进各种文化交流。</p>
						<p>同时，所有会员推荐的潜在会员必须通过俱乐部会籍资格委员会的"会籍面谈"及相应的资格审核后方可正式入会。苏州金鸡湖国际高尔夫俱乐部自2006年开业至今已有近700位会员，这个温馨国际化大家庭吸引着来自10个不同国家的成员，形成
了多元文化平等交流，并极具活力的俱乐部文化。</p>
						<div class="scLine"></div>
                        <p class="fontSize16">更多详情，请联系会籍专线：0512-6288 6868</p>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/membership.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
        
<?php get_footer(); ?>