<?php
/**
 * TEMPLATE NAME:地图
 */
get_header();
?>
        <div class="LnavBg minHB">
        	<a href="map.html" class="cur u1">
               地图
            </a>
        </div>
        <div class="LContLong">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            
            <div class="LContMiddle" style="min-height:450px;">
                <div class="cont" style="margin-top:10px;min-height:450px;">
                    <div class="sCont">
                    	<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="818" height="430">
                            <param name="movie" value="<?php echo get_template_directory_uri()?>/img/map1.swf">
                            <param name="quality" value="high">
                            <param name="wmode" value="Opaque">
                            <embed src="<?php echo get_template_directory_uri()?>/img/map.swf" width="818" height="430" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" wmode="Opaque"></embed>
                        </object>
                    </div>
        		</div>
            </div>
            <!--
            <div class="LContMiddle">
            	<div class="contTitle">地图</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    
                    	<a href="<?php echo get_template_directory_uri()?>/img/mapDetail.jpg" class="fancybox">
                    		<img src="<?php echo get_template_directory_uri()?>/img/mapDetail.jpg" />
                        </a>
                    
                    </div>
        		</div>
            </div>-->
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/map.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
			$('.fancybox').fancybox();
		});
		</script>
        
<?php get_footer(); ?>
