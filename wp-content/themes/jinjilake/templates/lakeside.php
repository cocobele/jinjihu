<?php
/**
 * TEMPLATE NAME:湖轩宴会厅
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=15" class="u1">
                专卖店
            </a>
            <a href="?page_id=50" class="u2">
            	Gary Player 雪茄吧
            </a>
            <a href="?page_id=52" class="u3">
                马可波罗西餐厅
            </a>
            <a href="?page_id=54" class="cur u4">
            	湖轩宴会厅
            </a>
            <a href="?page_id=56" class="u5">
                贵宾专属区
            </a>
            <a href="?page_id=58" class="u6">
            	会议室
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">湖轩宴会厅</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<p>俱乐部湖轩宴会厅，坐落于波光粼粼的金鸡湖畔，可欣赏湖面与草坪相伴之绝佳美景。每逢佳节，公司年会，或亲友生日，同学聚会，想要共享欢聚时光，湖轩中餐厅将精心为您提供私人定制的宴会服务，宽敞的餐厅最多可满足150人的需要！若是您想要
举办小型聚会，也可选择餐厅中的两个湖景包间——独墅阁与汀兰阁，坐拥湖畔风光，与亲友促膝举杯，闲话家常。</p>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/lakeside.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'none',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>