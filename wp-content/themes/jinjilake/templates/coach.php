<?php
/**
 * TEMPLATE NAME:教练简介
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=13" class="u1">
                学院简介
            </a>
            <a href="?page_id=43" class="cur u2">
            	教练简介
            </a>
            <a href="?page_id=45" class="u3">
            	课程简介
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">高尔夫教练简介</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar tempPic">
                    	<img src="<?php echo get_template_directory_uri()?>/img/coach1.png" />
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/coach4.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
        
<?php get_footer(); ?>