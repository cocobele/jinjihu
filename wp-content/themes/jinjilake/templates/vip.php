<?php
/**
 * TEMPLATE NAME:贵宾专属区
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=15" class="u1">
                专卖店
            </a>
            <a href="?page_id=50" class="u2">
            	Gary Player 雪茄吧
            </a>
            <a href="?page_id=52" class="u3">
                马可波罗西餐厅
            </a>
            <a href="?page_id=54" class="u4">
            	湖轩宴会厅
            </a>
            <a href="?page_id=56" class="cur u5">
                贵宾专属区
            </a>
            <a href="?page_id=58" class="u6">
            	会议室
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">贵宾专属区</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<p>位于会所三楼的贵宾专属区，VIP私属领地，专人尊贵服务，让您尽享奢华。贵宾专属区提供宴会、会议和更衣三个功能，宽敞明亮的宴会套间，最多可容纳20人共襄盛宴，整个房间以大气典雅的中式风格为主，巨大的落地窗可坐拥湖光绿地，将美景尽
收眼底，一览无遗，更配有舒适沙发，让您在茶余饭后不必走远便可与好友知己谈天品茗。</p>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/vip.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'SW',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'superSlide',
			sequenceMode: 'random',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>
