<?php
/**
 * TEMPLATE NAME:高尔夫学院
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=13" class="cur u1">
                学院简介
            </a>
            <a href="?coachtype=golfcoach" class="u2">
            	教练简介
            </a>
            <a href="?course=course" class="u3">
            	课程简介
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">高尔夫学院</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<p>金鸡湖高尔夫学院是会员和会员嘉宾完善球技的上佳之选，通过国际认证的PGA职业教练提供专业指导，一流的  教学设备帮助球友快速进步，提升球技。</p>
                        <p>从面面俱到的私人教学到轻松有趣的团体课程；从理论深厚的高球知识，到实地演练的打球策略，无论是初学入门，还是改良球技，都可以在这里找到丰富多样的课程选择。独具特色的最新视频分析系统采用最新的V1电脑软件为球友提供全面准确的挥杆分析。</p>
                        <p>学院另设球具维修中心旨在帮助会员把自己的球具调整到最适合的状态，包括杆身长度和角度的调整，挥杆重量 的调整以及握把和杆身的更换等等，专业培训的员工能满足球员对球具设备的所有需求。而由国际品牌合作提供 的高尔夫球杆"量身定做"服务，更有助于显著改善击球友的击球水平。</p>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/schoolBrief.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>