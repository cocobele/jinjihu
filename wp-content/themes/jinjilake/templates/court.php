<?php
/**
 * TEMPLATE NAME:球场
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=11" class="cur u1">
                球场简介
            </a>
            <a href="?page_id=41" class="u2">
            	GEO
            </a>
            <a href="?page_id=9" class="u3">
            	设计师
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">三种风格球洞</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<p>"金鸡湖高尔夫"世间少有将三种地貌完美融合在一起的高尔夫球场，只有在双湖合抱的自然恩赐面前，我们才有机会体验与自然沟通，精雕细琢这块璞玉，最终呈现出森林、湿地、丘陵三种风格各异的球场风景，包括三种风格各异的中途休息亭十座各俱特色的桥梁将苏州的、江南的乃至中国的文化浓缩其中。</p>
                        <div class="threePart">
                        	<li style="height:150px">
                            	<div class="partTop" style="height:110px">
                            		<img src="<?php echo get_template_directory_uri()?>/img/courtThumb.jpg" width="100%" />
                                </div>
                                <div class="partBottom" style="height:30px;text-align:center;">
                            		森林九洞
                                </div>
                            </li>
                            <li style="height:150px">
                            	<div class="partTop" style="height:110px">
                            		<img src="<?php echo get_template_directory_uri()?>/img/wetlandThumb.jpg" width="100%" />
                                </div>
                                <div class="partBottom" style="height:30px;text-align:center;">
                            		湿地九洞
                                </div>
                            </li>
                            <li style="height:150px">
                            	<div class="partTop" style="height:110px">
                            		<img src="<?php echo get_template_directory_uri()?>/img/beachThumb.jpg" width="100%" />
                                </div>
                                <div class="partBottom" style="height:30px;text-align:center;">
                            		海滨九洞
                                </div>
                            </li>
                            <div class="clearFloat"></div>
                        </div>
						<p>森林九洞：葱郁大树相拥合抱，展现江南罕有的森林气派；更有华东地区一流灯光球场设施，尽享夜间高尔夫的独特乐趣</p>
                        <p>海滨九洞：起伏多变的球道、随风翻滚的长草、Gary Player的特色沙坑，仿佛置身苏格兰</p>
                        <p>湿地九洞：球场与水景辉映，水波荡漾中，静静等待下一个流光异彩的考验</p>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/court.jpg','<?php echo get_template_directory_uri()?>/img/wetland.jpg','<?php echo get_template_directory_uri()?>/img/beach.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>
