<?php
/**
 * TEMPLATE NAME:马可波罗西餐厅
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=15" class="u1">
                专卖店
            </a>
            <a href="?page_id=50" class="u2">
            	Gary Player 雪茄吧
            </a>
            <a href="?page_id=52" class="cur u3">
                马可波罗西餐厅
            </a>
            <a href="?page_id=54" class="u4">
            	湖轩宴会厅
            </a>
            <a href="?page_id=56" class="u5">
                贵宾专属区
            </a>
            <a href="?page_id=58" class="u6">
            	会议室
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">马可波罗西餐厅</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<p>马可波罗西餐厅位于会所二楼，若您喜欢静谧幽静，可选择于室内享用佳肴；若您喜欢与自然为伴，可选择坐在室外，远眺高尔夫草地的绿茵森林，伴着阳光与微风享用美味。</p>
                        <p>俱乐部的餐饮团队用心为您烹制每一道餐点，选用最天然新鲜的食材以及最合理的膳食搭配方式，补充您在打球后所需的营养和能量。餐厅更会结合时令，不断推出适合节气的新的美味珍馐，满足您的味蕾，给您带来新的味觉体验。</p>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/marcoPolo.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>
