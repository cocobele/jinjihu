<?php
/**
 * TEMPLATE NAME:会所
 */
get_header();
?>       
        <div class="LnavBg">
        	<a href="?page_id=15" class="cur u1">
                专卖店
            </a>
            <a href="?page_id=50" class="u2">
            	Gary Player 雪茄吧
            </a>
            <a href="?page_id=52" class="u3">
                马可波罗西餐厅
            </a>
            <a href="?page_id=54" class="u4">
            	湖轩宴会厅
            </a>
            <a href="?page_id=56" class="u5">
                贵宾专属区
            </a>
            <a href="?page_id=58" class="u6">
            	会议室
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">专卖店</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<p>专卖店位于俱乐部会所一楼，在这里，不论是新款的球杆球具还是潮流时尚的服饰鞋帽，抑或是印有IMG以及俱乐部logo的专属装备，只要是您所能想到的高尔夫装备应有尽有。专卖店目前在售的品牌有:</p>
						<p>FootJoy，Titleist， Crest Link，  Oakley， Pro Simon， Number，Under Armour，  Helix，Honma，Abacus，Cleveland，Srixon……如此繁多的品牌，其中必能找到最适合您的那一款。</p>
						<p>为了让您享受更多权益，专卖店更会不时推出优惠折扣，让您购物愉快，打球舒心。如果您有任何关于购买方面的疑问或希望得到建议，欢迎您向我们的服务人员咨询，谢谢！</p>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/exclusiveShop.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
        
<?php get_footer(); ?>