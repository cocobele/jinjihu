<?php
/**
 * TEMPLATE NAME:IMG
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=2" class="u1">
                俱乐部简介
            </a>
            <a href="?page_id=33" class="u2">
            	MBI
            </a>
            <a href="?page_id=31" class="cur u3">
            	IMG
            </a>
           <a href="?honortype=honordetail" class="u4">
            	俱乐部荣誉
            </a>
            <a href="?namelisttype=namelistdetail" class="u5">
            	会员一杆进洞扬名榜
            </a>
            <a href="?champion=champion" class="u6">
            	俱乐部冠军榜
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle" style="margin-top:16px"><img src="<?php echo get_template_directory_uri()?>/img/IMGLogo1.png" /></div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<p>世界级高尔夫球场管理  国际级贵宾礼遇</p>
                        <p>创立于1960年，IMG是全球首屈一指的体育及娱乐管理集团，业务遍布全球，其高尔夫球场管理水准领跑亚洲。"金鸡湖高尔夫"由其全面管理，是IMG的中国旗舰俱乐部项目。IMG专家团队定期访问球会，并提供营运、维护以及营销方面的建议和支持。</p>
                        <p>通过IMG Prestige, "金鸡湖高尔夫"会员尊享逾90家优秀俱乐部的特别优惠（IMG Prestige 与亚洲、澳洲、中东、欧洲以及美国的一批高端俱乐部合作，为其会员互访提供尊享通道）</p>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/IMG.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
		<div class="foot">
        	<div class="language">
            	<img src="<?php echo get_template_directory_uri()?>/img/sanjiao.png" /> <a href="#">中文</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php bloginfo('url'); ?>/en">ENGLISH</a>
            </div>
            <div class="copyright">
            	<p>会籍专线：86-512-6288-6868</p>
                <p>版权所有：金鸡湖高尔夫 www.jinjilakegolf.com 2015</p>
            </div>
        </div>
    </div>
    
    <script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/main.js"></script>
</body>
</html>