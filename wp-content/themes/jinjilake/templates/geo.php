<?php
/**
 * TEMPLATE NAME:GEO
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=11" class="u1">
                球场简介
            </a>
            <a href="?page_id=41" class="cur u2">
            	GEO
            </a>
            <a href="?page_id=9" class="u3">
            	设计师
            </a>
        </div>
        <div class="LCont" style="width:933px;">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle" style="width:453px;">GEO认证</div>
                <div class="contLine" style="width:453px;"></div>
                <div class="cont" style="width:453px;">
                    <div class="sCont mCustomScrollbar">
                    	<p>2011年8月，"金鸡湖高尔夫"成功通过GEO（欧洲高尔夫环境组织）的认证，成为中国大陆第一家成功通过该项认证的高尔夫俱乐部，2014年又再次通过复评，在打造环保型球场，走可持续发展道路的过程中，迈出了坚实的一步。</p>
                        <p>为了保持GEO认证后球场的可持续性发展，GEO需要每三年对已经通过认证的球场进行复评，其标准主要围绕水、能源与资源、环境质量、景观与生态、人文社区、产品与供应链六大板块进行。因此，在获得认证后的三年里，"金鸡湖高尔夫"也一直在根据GEO的要求不断的提升， GEO认证官Micah Woods博士在到俱乐部进行实地访问、审核之后，对俱乐部给出了极高的评价，他所提出的改善意见也得到了顺利的解决。在接下来的时间里，俱乐部将继续努力，不断致力于环境的改善和提升，不仅仅为会员及嘉宾提供一个健康的高尔夫环境，也为环保事业尽绵薄之力！</p>
                    </div>
        		</div>
                <div class="geoCerti mCustomScrollbar">
                	<img src="<?php echo get_template_directory_uri()?>/img/geoCerti.jpg" />
                </div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/geo.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>
