<?php
/**
 * TEMPLATE NAME:会员制俱乐部
 */
get_header();
?>

        
        <div class="LnavBg">
        	<a href="?page_id=2" class="cur u1">
                俱乐部简介
            </a>
            <a href="?page_id=33" class="u2">
            	MBI
            </a>
            <a href="?page_id=31" class="u3">
            	IMG
            </a>
            <a href="?honortype=honordetail" class="u4">
            	俱乐部荣誉
            </a>
            <a href="?namelisttype=namelistdetail" class="u5">
            	会员一杆进洞扬名榜
            </a>
            <a href="?champion=champion" class="u6">
            	俱乐部冠军榜
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">会员制俱乐部</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<p>苏州金鸡湖国际高尔夫俱乐部位于苏州工业园区金鸡湖与独墅湖之间，由高尔夫"黑骑士"Gary Player 签名设计， 球场结合了林克斯、湿地和灯光森林三种迥异的风格，每辆球车上都配备了全球卫星定位系统(GPS)，结合球员服务区、宴会厅、
西餐厅、雪茄吧及贵宾专属特区组成的 1万平米私人会馆，视为现代城市高尔夫的典范之作。</p>
						<p>"金鸡湖高尔夫"为IMG全面管理的旗舰项目，以世界领先的管理经验为会员提供一流的服务。俱乐部始终坚持严格的MBI入会制及会籍面谈制，让会员亲身感受到无微不至的亲切服务。</p>
                        <p>2010年，俱乐部举办了有史以来最为成功的一届Volvo中国公开赛，这也是首次由中国的高尔夫俱乐部举办的经欧 巡赛和同一公开赛，这也是首次由中国的高尔夫俱乐部举办的经欧巡赛和同一亚洲联合认证批准的国际级锦标赛，其高水准的球场设
】施及俱乐部服务得到了国内外职业球手及业界、媒体的高度评价。</p> 
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/brief1.jpg','<?php echo get_template_directory_uri()?>/img/brief2.jpg','<?php echo get_template_directory_uri()?>/img/brief.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>
