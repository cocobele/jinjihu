<?php
/**
 * TEMPLATE NAME:MBI
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=2" class="u1">
                俱乐部简介
            </a>
            <a href="?page_id=33" class="cur u2">
            	MBI
            </a>
            <a href="?page_id=31" class="u3">
            	IMG
            </a>
            <a href="?honortype=honordetail" class="u4">
            	俱乐部荣誉
            </a>
            <a href="?namelisttype=namelistdetail" class="u5">
            	会员一杆进洞扬名榜
            </a>
            <a href="?champion=champion" class="u6">
            	俱乐部冠军榜
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle" style="margin-top:12px;">
                	<p>真正的纯会员俱乐部</p>
                    <p>严格的MBI入会制及会籍面谈制</p>
                </div>
                <div class="contLine" style="margin-top:92px"></div>
                <div class="cont" style="margin-top:106px">
                    <div class="sCont mCustomScrollbar">
                    	<p>苏州金鸡湖国际高尔夫俱乐部是国内独具特色的纯正会员专属俱乐部，真正保障每一位会员的专属权益。 严格的会员邀请制(MBI)和会籍面谈制更彰显会员身份的尊贵特性。</p>
						<p>"会员邀请入会制"(MBI)要求新会员必须由现有会员推荐入会，这是世界顶级高尔夫俱乐部所采用的会员招募制度，此举不仅扩大了会员专属权益，更进一步提升了会籍的无形价值。</p>
						<p>以俱乐部总经理为首的四位管理层团队组成"会籍资格委员会"，对每一位申请入会者进行单独 "会籍面谈"及审核，使申请者在入会前较好地理解俱乐部的规范制度，明确球会的定位、管理和服务承诺，并切身感受到无微不至的会员服务。</p>
                        <div class="scLine"></div>
                        <p class="fontSize16">会籍专线：0512-6288 6868</p>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/MBI.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
        
<?php get_footer(); ?>
