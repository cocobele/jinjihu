<?php
/**
 * TEMPLATE NAME:最新动态
 */
get_header();
?>       
        
        <div class="LnavBg">
        	<a href="?cat=3" class="cur u1">
                俱乐部动态
            </a>
            <a href="?ejournaltype=ejournaldetail" class="u2">
            	电子会刊
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">俱乐部动态</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar newsList">
                    	<?php while (have_posts()):the_post()?>
                        	<div class="postTitle"><?php the_title()?></div>
                            <p>
                            	<?php the_content(); ?>
                            </p>
                        <?php endwhile;	?>
                        <a href="?cat=3">
                        	<p class="marTop10 fontSize16">返回新闻列表</p>
                        </a>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/news.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
        
<?php get_footer(); ?>