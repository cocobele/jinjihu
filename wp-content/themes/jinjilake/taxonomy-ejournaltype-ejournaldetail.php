<?php
/**
 * TEMPLATE NAME:电子会刊
 */
get_header();
?>       
        
        <div class="LnavBg">
        	<a href="?cat=3" class="u1">
                俱乐部动态
            </a>
            <a href="?ejournaltype=ejournaldetail" class="cur u2">
            	电子会刊
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle" style="margin-top:14px">
                	<ul>电子会刊</ul>
                	<ul id="yearList" class="mCustomScrollbar" data-mcs-axis="x">
                       <div id="yearListDiv">
                       		
                       </div>
                    </ul>
                </div>
                
                <div class="contLine" style="margin-top:80px"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar"  data-mcs-axis="y">
                    	<ul class="threePart">
                        	<script language="javascript">
								function sortNumber(b, a)
								{
								return a - b
								}
								var yearArray = new Array();
								function addYear(y){
									var b = true;
									for(var i=0;i<yearArray.length;i++){
										if(y==yearArray[i]){
											b = false;
											break;
										}
									}
									if(b){
										yearArray.push(y);
									}
								}
								
							</script>
                            <?php while (have_posts()):the_post();?>
                                <li class="alignCenter <?php echo get_post_meta(get_the_ID(),'year',true)?> yearPicDetail" style="height:230px;">
                                		<div class="partTop">
                                            <a class="fancybox-buttons" data-fancybox-group="<?php the_title(); ?>" href="<?php echo get_field('cover') ?>">
                                                <img src="<?php echo get_field('cover') ?>" />
                                            </a>
                                            <?php $images = get_field('eimages');
                                                  if (!empty($images)):
                                            ?>
												<?php foreach ($images as $key => $image):?>
                                                    <a class="fancybox-buttons" data-fancybox-group="<?php the_title(); ?>" href="<?php echo $image[url]?>">
                                                    </a>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </div>
                                        <div class="partBottom" style="height:40px;">
                                    		<?php echo get_post_meta(get_the_ID(),'bookname',true)?>
                                    	</div>
                                </li>
                            	<script language="javascript">
									addYear(<?php echo get_post_meta(get_the_ID(),'year',true)?>);
								</script>
                            <?php endwhile;?>
                            <div class="clearFloat"></div>
                        </ul>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/eJournal.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
					$('.fancybox-buttons').fancybox({
						padding:0,
						margin:0,

						autoSize:true,
						openEffect  : 'elastic',
						closeEffect : 'elastic',
		
						prevEffect : 'fade',
						nextEffect : 'fade',
		
						closeBtn  : false,
		
						helpers : {
							title : {
								type : 'inside'
							},
							buttons	: {}
						},
					});
					
			yearArray.sort(sortNumber);
			var yldw = yearArray.length * 47;
			$("#yearListDiv").css('width',yldw);
			for(var i=0;i<yearArray.length;i++){
				var myli = "<li>"+yearArray[i]+"</li>";
				$("#yearListDiv").append(myli);
			}
			
			$('#yearListDiv li').mousedown(function(){
				$("#yearListDiv li").each(function(){
					$(this).css('background','none').css('color','#e3c166');
				});
				$(this).css('background-color','#fcebc5').css('color','#352602');
				
				$(".yearPicDetail").each(function(){
					$(this).css('display','none');
				});
				var showli = "." + $(this).text();
				$(showli).each(function(){
					$(this).css('display','block');
				});
			});
			
			$('#yearListDiv li:first-child').mousedown();
			
			
		});
		</script>

<?php get_footer(); ?>