<?php
/**
 * TEMPLATE NAME:扬名榜
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=2" class="u1">
                俱乐部简介
            </a>
            <a href="?page_id=33" class="u2">
            	MBI
            </a>
            <a href="?page_id=31" class="u3">
            	IMG
            </a>
            <a href="?page_id=35" class="u4">
            	俱乐部荣誉
            </a>
            <a href="?page_id=37" class="cur u5">
            	会员一杆进洞扬名榜
            </a>
            <a href="?page_id=39" class="u6">
            	俱乐部冠军榜
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle alignCenter">一杆进洞扬名榜</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar alignCenter">
                    	<table class="bordered">
                        	<tr>
                            	<th>
                                	序号
                                </th>
                                <th>
                                	姓名
                                </th>
                                <th>
                                	日期
                                </th>
                                <th>
                                	场地
                                </th>
                                <th>
                                	球洞
                                </th>
                                <th>
                                	码距
                                </th>
                            </tr>
                            <tr>
                            	<td>
                                	01
                                </td>
                                <td>
                                	金龙喆
                                </td>
                                <td>
                                	2006-7-30
                                </td>
                                <td>
                                	森林
                                </td>
                                <td>
                                	#4
                                </td>
                                <td>
                                	212
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                	02
                                </td>
                                <td>
                                	权贞烨
                                </td>
                                <td>
                                	2007-1-20
                                </td>
                                <td>
                                	湿地
                                </td>
                                <td>
                                	#8
                                </td>
                                <td>
                                	109
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                	03
                                </td>
                                <td>
                                	周殿强
                                </td>
                                <td>
                                	2007-3-6
                                </td>
                                <td>
                                	湿地
                                </td>
                                <td>
                                	#6
                                </td>
                                <td>
                                	151
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                	04
                                </td>
                                <td>
                                	尤玉国
                                </td>
                                <td>
                                	2007-3-25
                                </td>
                                <td>
                                	森林
                                </td>
                                <td>
                                	#4
                                </td>
                                <td>
                                	185
                                </td>
                            </tr>
                            <tr>
                            	<td>05</td><td>李文龙</td><td>2007-4-20</td><td>海滨</td><td>#8</td><td>170</td>
                            </tr>
                            <tr>
                            	<td>06</td><td>刘贵国</td><td>2007-7-5</td><td>森林</td><td>#4</td><td>212</td>
                            </tr>
                            <tr>
                            	<td>07</td><td>金相圭</td><td>2007-10-26</td><td>湿地</td><td>#6</td><td>151</td>
                            </tr>
                            <tr>
                            	<td>08</td><td>田洛彰</td><td>2008-4-5</td><td>森林</td><td>#8</td><td>155</td>
                            </tr>
                            <tr>
                            	<td>09</td><td>尹剑平</td><td>2008-5-2</td><td>海滨</td><td>#4</td><td>160</td>
                            </tr>
                            <tr>
                            	<td>10</td><td>金南一</td><td>2008-5-25</td><td>森林</td><td>#8</td><td>170</td>
                            </tr>
                            <tr>
                            	<td>11</td><td>顾薛佳</td><td>2008-8-17</td><td>海滨</td><td>#8</td><td>168</td>
                            </tr>
                            <tr>
                            	<td>12</td><td>金相圭</td><td>2009-3-8</td><td>湿地</td><td>#8</td><td>156</td>
                            </tr>
                        </table>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/nameList.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
                
<?php get_footer(); ?>
