<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="edge" />
<meta name="viewport" content="width=device-width,initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<title><?php wp_title( '_', true, 'right' ); ?></title>
<meta name="keywords" content="金鸡湖高尔夫" />
<meta name="description" content="金鸡湖高尔夫" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/style.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/bgstretcher.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/css/jquery.mCustomScrollbar.css">
<link rel="shortcut icon" href="<?php echo get_template_directory_uri()?>/favicon.ico"/>

<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/lib/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/bgstretcher.js"></script>
<script src="<?php echo get_template_directory_uri()?>/js/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/source/jquery.fancybox.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/source/jquery.fancybox.css?v=2.1.5" media="screen" />
    
    <!-- Add Button helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
</head>

<body>
	<div class="wrap">      
        <div class="logo">
        	<a href="<?php bloginfo('url'); ?>"><img src="<?php echo get_template_directory_uri()?>/img/logobk.png" name="Logo" id="Logo" border=0 /></a>
        </div>
        <?php wp_nav_menu(array('theme_location'=>'primary'))?>