<?php


require 'includes/shortcodes.php';

/*Theme setup*/
add_action('init', 'theme_setup');
function theme_setup(){
	//前台取消admin bar
	add_action ( 'show_admin_bar', '__return_false' );
	add_theme_support( 'post-thumbnails' );
	
	//注册缩略图
	add_image_size('galleryPic',150,100,false);
	
	//注册导航
	register_nav_menus(array(
		'primary'	=>	'Main menu',
		//'quicklink'	=>	'Quick Link',
		//'footer'	=>	'footer Menu',
	));
}

add_filter('jpeg_quality', 'set_jpeg_quality');
function set_jpeg_quality(){
	return 100;
}

/**
 * Load scripts & styles;
 */
/**
 * 加载必须的样式表及js库
 */
add_action ( 'wp_enqueue_scripts', 'theme_scripts_styles', '100' );
function theme_scripts_styles() {
	
	wp_enqueue_style ( 'style', get_template_directory_uri () . '/style.css' );
	
	wp_enqueue_script ( 'jquery');
	wp_enqueue_script ( 'ms', get_template_directory_uri () . '/js/masterslider.min.js');
}


class main_menu{
	function walk($menus){
		$output = '<div class="home-menu"><ul>';
		foreach ($menus as $menu){
			if (!$menu->menu_item_parent){
				$classes = join(' ', array_filter($menu->classes));
				$output.= '<li class="'.$classes.'"><a href="'.$menu->url.'"></a><span>'.$menu->title.'</span></li>';
			}
		}
		$output .= '</ul></div>';
		return $output;
	}
}

/**
 * Create a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since Twenty Fourteen 1.0
 *
 * @global int $paged WordPress archive pagination page count.
 * @global int $page  WordPress paginated post page count.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function theme_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name', 'display' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
		$title = "$title $sep " . sprintf( __( 'Page %s', 'twentyfourteen' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'theme_title', 10, 2 );


/**
 * 获取当前菜单
 */
function get_current_menu($menus){
	foreach ($menus as $key => $menu){
		if ($menu->current === true){
			$GLOBALS['current_menu'] = $menu;
			break;
		}elseif ($menu->current_item_parent==true){
			$GLOBALS['current_menu'] = $menu;
			break;
		}
		
		if (get_post_type()=='brand' && $menu->title=='材料保证'){
			$menus[$key]->classes[] = 'current-menu-ancestor';
			$GLOBALS['current_menu'] = $menu;
		}
		if (is_tax('case-author') && $menu->title=='设计团队'){
			$menus[$key]->classes[] = 'current-menu-ancestor';
			$GLOBALS['current_menu'] = $menu;
		}
	}
	
	return $menus;
}
add_filter('wp_nav_menu_objects', 'get_current_menu');

/**
 * 将附件中文名转英文名
 * @param unknown_type $tabs
 * @return string
 */
add_filter('sanitize_file_name','chinese_char');
function chinese_char($string){
	return preg_replace_callback('/[\x{0080}-\x{ffff}]/u','get_hex_by_callback',$string);
}

function get_hex_by_callback($string){
	$string = (string)$string[0];
	return bin2hex($string);
}

function get_child_team($parent_id=0,$taxonomy=''){
	$terms = get_queried_object();
	if ($parent_id==0){
		$parent_id = $terms->term_id;
	}
	
	if (empty($taxonomy)){
		$taxonomy = $terms->taxonomy;
	}
	
	$argv = array(
			'orderby'       =>  'term_order',
			'hide_empty'    => false
	);
	$categories = get_terms($taxonomy, $argv);
	foreach ($categories as $child){
		if ($child->parent == $parent_id){
			$result[] = $child;
		}
	}
	return $result;
}

/**
 * 截取字符串
 * @param unknown_type $sourcestr
 * @param unknown_type $cutlength
 * @param unknown_type $suffix
 * @return unknown|string
 */
function short($sourcestr,$cutlength=0,$suffix=' &hellip;'){
	$str_length = strlen($sourcestr);
	if($str_length <= $cutlength) {
		return $sourcestr;
	}
	$returnstr='';
	$n = $i = $noc = 0;
	while($n < $str_length) {
		$t = ord($sourcestr[$n]);
		if($t == 9 || $t == 10 || (32 <= $t && $t <= 126)) {
			$i = 1; $n++; $noc++;
		} elseif(194 <= $t && $t <= 223) {
			$i = 2; $n += 2; $noc += 2;
		} elseif(224 <= $t && $t <= 239) {
			$i = 3; $n += 3; $noc += 2;
		} elseif(240 <= $t && $t <= 247) {
			$i = 4; $n += 4; $noc += 2;
		} elseif(248 <= $t && $t <= 251) {
			$i = 5; $n += 5; $noc += 2;
		} elseif($t == 252 || $t == 253) {
			$i = 6; $n += 6; $noc += 2;
		} else {
			$n++;
		}
		if($noc >= $cutlength) {
			break;
		}
	}
	if($noc > $cutlength) {
		$n -= $i;
	}
	$returnstr = substr($sourcestr, 0, $n);


	if ( substr($sourcestr, $n, 6)){
		$returnstr = $returnstr . $suffix;//超过长度时在尾处加上省略号
	}
	return $returnstr;
}

add_filter( 'pre_get_posts', 'wpse170243_custom_ppp' );
function wpse170243_custom_ppp($q ) {
	if( !is_admin() && $q->is_main_query() ) {
		//var_dump($q->query);
		if ($q->is_tax( 'coachtype') || $q->query['post_type']=='coach'){
			//var_dump($q->query);
			$q->set( 'posts_per_page', 1 );
		}elseif ($q->is_tax( 'photogallerytype') || $q->query['post_type']=='photogallery'){
			$q->set( 'posts_per_page', 9999);
		}elseif ($q->is_tax('honortype')){
			$q->set( 'posts_per_page', 9999);
		}elseif ($q->is_tax('material-type')){
			$q->set( 'posts_per_page', 4);
		}elseif ($q->query['post_type'] == 'brand'){
			$q->set( 'posts_per_page', 2);
		}elseif ($q->query['post_type'] == 'construction'){
			$q->set( 'posts_per_page', 2);
		}
		$q->set('orderby','modified');
	}
	return $q;
}

function par_pagenavi($range = 9){

	global $paged, $wp_query;

	if ( !$max_page ) {$max_page = $wp_query->max_num_pages;}

	if($max_page > 1){if(!$paged){$paged = 1;}

	

	

    if($max_page > $range){

		if($paged < $range){for($i = 1; $i <= ($range + 1); $i++){echo "<a href='" . get_pagenum_link($i) ."'";

		if($i==$paged)echo " class='current'";echo ">$i</a>";}}

    elseif($paged >= ($max_page - ceil(($range/2)))){

		for($i = $max_page - $range; $i <= $max_page; $i++){echo "<a href='" . get_pagenum_link($i) ."'";

		if($i==$paged)echo " class='current'";echo ">$i</a>";}}

	elseif($paged >= $range && $paged < ($max_page - ceil(($range/2)))){

		for($i = ($paged - ceil($range/2)); $i <= ($paged + ceil(($range/2))); $i++){echo "<a href='" . get_pagenum_link($i) ."'";if($i==$paged) echo " class='current'";echo ">$i</a>";}}}

    else{for($i = 1; $i <= $max_page; $i++){echo "<a href='" . get_pagenum_link($i) ."'";

    if($i==$paged)echo " class='current'";echo ">$i</a>";}}

	

    }

}

function devpress_login_form_shortcode() {
	if ( is_user_logged_in() )
		return '';
 
	return wp_login_form( array( 'echo' => false, 'remember' => false ) );
}
 
function devpress_add_shortcodes() {
	add_shortcode( 'my-login-form', 'devpress_login_form_shortcode' );
}
 
add_action( 'init', 'devpress_add_shortcodes' );