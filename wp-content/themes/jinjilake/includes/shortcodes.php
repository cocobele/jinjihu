<?php



if (!function_exists('screen')){
	
	function screen($atts,$content=null){
		extract(shortcode_atts(array(
			'id'=>'',
			'style'=>'',
			
		), $atts));

		$render = '<div class="screen-section">'.do_shortcode($content).'</div>';
		return $render;
	}
	
	add_shortcode('screen', 'screen');
}