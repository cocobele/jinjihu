<?php
/**
 * TEMPLATE NAME:俱乐部冠军榜
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=2" class="u1">
                俱乐部简介
            </a>
            <a href="?page_id=33" class="u2">
            	MBI
            </a>
            <a href="?page_id=31" class="u3">
            	IMG
            </a>
            <a href="?honortype=honordetail" class="u4">
            	俱乐部荣誉
            </a>
            <a href="?namelisttype=namelistdetail" class="u5">
            	会员一杆进洞扬名榜
            </a>
            <a href="?champion=champion" class="cur u6">
            	俱乐部冠军榜
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle alignCenter" >俱乐部冠军榜</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<?php while ( have_posts() ) : the_post(); ?>
                                    <?php the_content() ?>
                        <?php endwhile;	?>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/clubChampion1.jpg','<?php echo get_template_directory_uri()?>/img/clubChampion2.jpg','<?php echo get_template_directory_uri()?>/img/clubChampion.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>
