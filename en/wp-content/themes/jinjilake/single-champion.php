<?php
/**
 * TEMPLATE NAME:俱乐部冠军榜
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=2" class="u1">
                A Private Club
            </a>
            <a href="?page_id=33" class="u2">
            	MBI
            </a>
            <a href="?page_id=31" class="u3">
            	IMG
            </a>
            <a href="?honortype=honordetail" class="u4">
            	Honour List
            </a>
            <a href="?namelisttype=namelistdetail" class="u5">
            	Hole in One List
            </a>
            <a href="?champion=champion" class="cur u6">
            	Club Champions
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle alignCenter" >Club Champions</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<?php while ( have_posts() ) : the_post(); ?>
                                    <?php the_content() ?>
                        <?php endwhile;	?>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/clubChampion1.jpg','<?php echo get_template_directory_uri()?>/img/clubChampion2.jpg','<?php echo get_template_directory_uri()?>/img/clubChampion.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>
