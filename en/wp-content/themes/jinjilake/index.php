<?php

get_header();
?>

<div class="entry">
	
	<div class="wrap">
		
		
		
		<div class="news-sub-title"></div>
		<div class="news" id="news-list">
			<ul>
			<?php while (have_posts()):the_post()?>
				<li>
					<span>
						<em><?php echo get_the_date('d')?></em>
						<b><?php echo get_the_date('Y-m')?></b>
					</span>
					<h4>
						<a href="<?php the_permalink()?>"><?php the_title()?></a>
					</h4>
					<div class="description"><?php echo short(get_the_excerpt(),100)?></div>
				</li>
			<?php endwhile;?>
			</ul>
		</div>
		
		<div class="news-content hide" id="news-content">
			
			<div class="new-content-close"></div>
			<div class="new-detail">
				
			</div>
		</div>
		
		
		<?php 
			global $wp_query;
		?>
		
		<a class="more news-more" data-append=".news ul" href="<?php echo get_next_posts_page_link($wp_query->max_num_pages); ?>"></a>
		
	</div>

</div>
<?php get_footer()?>