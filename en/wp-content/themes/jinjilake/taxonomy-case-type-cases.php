<?php

get_header();
?>


<div class="entry">
	
	<div class="banner">
		<img src="<?php echo get_template_directory_uri()?>/images/case-banner.jpg">
	</div>
	
	<div class="wrap">
		
		<div class="case">
			<div class="case-total">
				已找到【<?php global $wp_query; echo $wp_query->found_posts?>】个符合条件的案例作品
			</div>
			
			<div class="layout-box">
				<ul class="one-four column">
					<?php while (have_posts()):the_post();?>
					
					<li>
						<a href="<?php the_permalink()?>"><?php the_post_thumbnail('case-list')?></a>
						<h4 class="case-title"><?php the_title();?></h4>
						
						<div class="excerpt">
							<i class="icon-user"></i>
							<span>
								<?php 
									$designeies = get_the_terms($post, 'case-author');
									if (!empty($designeies)){
										echo $designeies[0]->name;
									}
								?>
							</span>
							<i class="icon-tips"></i>
							<span>
								风格：<?php echo get_post_meta(get_the_ID(),'design_style',true)?>
							</span>
							<span>
								面积：<?php echo get_post_meta(get_the_ID(),'acreage',true)?>
							</span>
						</div>
						
					</li>
					<?php endwhile;?>
				</ul>
			</div>
			
			<?php 
				global $wp_query;
			?>
			
			<a class="more" data-append=".layout-box > ul" href="<?php echo get_next_posts_page_link($wp_query->max_num_pages); ?>">
				<span><em>查看更多</em></span>
			</a>
		</div>
	</div>
</div>


<?php get_footer()?>