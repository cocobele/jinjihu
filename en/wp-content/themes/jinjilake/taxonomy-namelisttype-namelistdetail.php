<?php
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=2" class="u1">
                A Private Club
            </a>
            <a href="?page_id=33" class="u2">
            	MBI
            </a>
            <a href="?page_id=31" class="u3">
            	IMG
            </a>
            <a href="?honortype=honordetail" class="u4">
            	Honour List
            </a>
            <a href="?namelisttype=namelistdetail" class="cur u5">
            	Hole in One List
            </a>
            <a href="?champion=champion" class="u6">
            	Club Champions
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle alignCenter">Hole in One List</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar alignCenter">
                    	<table class="bordered">
                        	<tr>
                            	<th>
                                	No.
                                </th>
                                <th>
                                	Name
                                </th>
                                <th>
                                	Date
                                </th>
                                <th>
                                	Course
                                </th>
                                <th>
                                	Hole
                                </th>
                                <th>
                                	Yard
                                </th>
                            </tr>
                            <?php   $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
									$args=array(
									'namelisttype' => 'namelistdetail',
									'meta_key' => 'num',
									'paged' => $paged,
									'orderby' => 'meta_value',
									'posts_per_page' => 10,
									'order' => 'ASC'
									);
									query_posts($args);
							while (have_posts()):the_post();?>
                            	<tr>
                                	<td>
                                    	<?php echo get_post_meta(get_the_ID(),'num',true)?>
                                    </td>
                                    <td>
                                    	<?php echo get_post_meta(get_the_ID(),'name',true)?>
                                    </td>
                                    <td>
                                    	<?php echo get_post_meta(get_the_ID(),'date',true)?>
                                    </td>
                                    <td>
                                    	<?php echo get_post_meta(get_the_ID(),'area',true)?>
                                    </td>
                                    <td>
                                    	<?php echo get_post_meta(get_the_ID(),'hole_num',true)?>
                                    </td>
                                    <td>
                                    	<?php echo get_post_meta(get_the_ID(),'distance',true)?>
                                    </td>
                                </tr>
                            <?php endwhile;?>
                            <!--
                            <tr>
                            	<td>
                                	01
                                </td>
                                <td>
                                	金龙喆
                                </td>
                                <td>
                                	2006-7-30
                                </td>
                                <td>
                                	森林
                                </td>
                                <td>
                                	#4
                                </td>
                                <td>
                                	212
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                	02
                                </td>
                                <td>
                                	权贞烨
                                </td>
                                <td>
                                	2007-1-20
                                </td>
                                <td>
                                	湿地
                                </td>
                                <td>
                                	#8
                                </td>
                                <td>
                                	109
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                	03
                                </td>
                                <td>
                                	周殿强
                                </td>
                                <td>
                                	2007-3-6
                                </td>
                                <td>
                                	湿地
                                </td>
                                <td>
                                	#6
                                </td>
                                <td>
                                	151
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                	04
                                </td>
                                <td>
                                	尤玉国
                                </td>
                                <td>
                                	2007-3-25
                                </td>
                                <td>
                                	森林
                                </td>
                                <td>
                                	#4
                                </td>
                                <td>
                                	185
                                </td>
                            </tr>
                            <tr>
                            	<td>05</td><td>李文龙</td><td>2007-4-20</td><td>海滨</td><td>#8</td><td>170</td>
                            </tr>
                            <tr>
                            	<td>06</td><td>刘贵国</td><td>2007-7-5</td><td>森林</td><td>#4</td><td>212</td>
                            </tr>
                            <tr>
                            	<td>07</td><td>金相圭</td><td>2007-10-26</td><td>湿地</td><td>#6</td><td>151</td>
                            </tr>
                            <tr>
                            	<td>08</td><td>田洛彰</td><td>2008-4-5</td><td>森林</td><td>#8</td><td>155</td>
                            </tr>
                            <tr>
                            	<td>09</td><td>尹剑平</td><td>2008-5-2</td><td>海滨</td><td>#4</td><td>160</td>
                            </tr>
                            <tr>
                            	<td>10</td><td>金南一</td><td>2008-5-25</td><td>森林</td><td>#8</td><td>170</td>
                            </tr>
                            <tr>
                            	<td>11</td><td>顾薛佳</td><td>2008-8-17</td><td>海滨</td><td>#8</td><td>168</td>
                            </tr>
                            <tr>
                            	<td>12</td><td>金相圭</td><td>2009-3-8</td><td>湿地</td><td>#8</td><td>156</td>
                            </tr>
                            -->
                        </table>
                        <div class="page_navi"><?php par_pagenavi(9); ?></div>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/nameList.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
                
<?php get_footer(); ?>
