<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="edge" />
<meta name="viewport" content="width=device-width,initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<title><?php wp_title( '_', true, 'right' ); ?></title>
<meta name="keywords" content="金鸡湖高尔夫" />
<meta name="description" content="金鸡湖高尔夫" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/style.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/bgstretcher.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/css/jquery.mCustomScrollbar.css">
<link rel="shortcut icon" href="<?php echo get_template_directory_uri()?>/favicon.ico"/>

<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/jwplayer/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/bgstretcher.js"></script>
<script src="<?php echo get_template_directory_uri()?>/js/jquery.mCustomScrollbar.concat.min.js"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/test1.js"></script>

<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/source/jquery.fancybox.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/source/jquery.fancybox.css?v=2.1.5" media="screen" />
    
    <!-- Add Button helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
</head>

<body>

	<div class="wrap">      
    	<div class="VideoIndex">
            <div id='myplayer'></div>
        </div>
        <div class="logo">
        	<a href="<?php bloginfo('url'); ?>"><img src="<?php echo get_template_directory_uri()?>/img/logobk.png" name="Logo" id="Logo" border=0 /></a>
        </div>
        <?php wp_nav_menu(array('theme_location'=>'primary'))?>
        
        
        <div class="LnavBg">
        	<a href="index.html" class="cur u1">
                Log In
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<?php
                   if ( is_user_logged_in() ) {
                ?>
                <?php global $current_user;
					 get_currentuserinfo();
					 //echo '用户名： ' . $current_user->user_login . "\n";
					 // echo '用户邮箱： ' . $current_user->user_email . "\n";
					 //echo '名字： ' . $current_user->user_firstname . "\n";
					 // echo '姓氏： ' . $current_user->user_lastname . "\n";
					 // echo '公开显示名： ' . $current_user->display_name . "\n";
					 // echo '用户 ID：' . $current_user->ID . "\n";
				?>
                		<div class="contTitle" style="margin-top:12px;">
                        	<p><div class="welcome">Welcome，<?php echo $current_user->user_login ?> </div>
								<?php $url_this = 'http://'.$_SERVER['SERVER_NAME'];?>
                                <div class="logout"><a href="<?php echo wp_logout_url($url_this); ?>">Log Out</a></div>
                                <div class="clearFloat"></div>
                            </p>
                            <p class="fontSize14">Welcome to JinJi lake golf club</p>
                        </div>
                        <div class="contLine" style="margin-top:92px"></div>
                        <div class="cont" style="margin-top:106px">
                            <div class="sCont mCustomScrollbar">
                            	<ul class="threePart"> 
									<?php   $args=array(
                                        'photogallerytype' => 'photogallerydetail',
                                        'meta_key' => 'gallerydate',
                                        'orderby' => 'meta_value',
                                        'posts_per_page' => 9999,
                                        'order' => 'DESC'
                                        );
                                        query_posts($args);
                                        while (have_posts()):the_post();?>
                                            <li class="alignCenter" style="height:135px;">
                                                <div class="partTop" style="height:90px;">
                                                    <a href="<?php echo get_permalink(); ?>">
                                                        <img src="<?php echo get_field('photoindex') ?>" />
                                                    </a>
                                                </div>
                                                <div class="partBottom" style="height:35px; line-height:16px;">
                                                    <?php the_title() ?>
                                                </div>
                                            </li>
                                    <?php endwhile;?>
                                    <?php wp_reset_query();?>
                                	<div class="clearFloat"></div>
                        		</ul>
                            </div>
                      	</div>
                <?php } else { ?>
							<div class="contTitle alignCenter">Log In</div>
                            <div class="contLine"></div>
                            <div class="cont">
                                <div class="sCont mCustomScrollbar indexscont">
                                    <p>If you are the Club member and have created an account to access the Member Only page,please login here</p>
                                    <p class="marTop10">
                                    	<?php echo do_shortcode('[my-login-form]')?>
                                        
                                    </p>
                                    <div class="scLine"></div>
                                    <p class="fontSize14">
                                        Forget password:
                                    </p>
                                    <p>
                                        If you have registerd and forgot your password，Please fill your e-mail address below and click the Submit button.
                                    </p>
                                    <p>
                                        <?php echo do_shortcode('[contact-form-7 id="160" title="找回密码"]')?>
                                    </p>
                                    <p>
                                        We will identify your email within 24 hours. Any problem, please dail Membership Hotline:：86-512-6288 6868
                                    </p>
                                </div>
                            </div>	
           		<?php } ?> 
            	
            </div>
            <div class="LContFoot"></div>
        </div>
        
        <?php
            if ( is_user_logged_in() ) { 
        ?>
        	<?php while (have_posts()):the_post();?>
            <div class="photogallery mCustomScrollbar">
                <ul class="galleryTitle"><?php the_title();?></ul>
                <ul class="galleryLine"></ul>
                <ul class="galleryShow">
                	<?php $images = get_field('gallery');
                          if (!empty($images)):
                     ?>
                     	<?php foreach ($images as $key => $image):?>
                             
                              	<li>
                                 <a class="fancybox-buttons" data-fancybox-group="gallery" href="<?php echo $image[url]?>">
                                	<img src="<?php echo $image['sizes']['galleryPic']?>" />
                                 </a>
                                </li>
                              
                        <?php endforeach;?>
                        <div class="clearFloat"></div>
                        <?php endif;?>
                </ul>
            </div>
            <?php endwhile;?>
        <?php } ?> 
        
        <div class="foot">
        	<div class="language">
            	<img src="<?php echo get_template_directory_uri()?>/img/sanjiao.png" /> <a href="http://jinjilake.eogor.cn">中文</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">ENGLISH</a>
            </div>
            <div class="memberLogo">
            	<div class="moButton">
            		<img src="<?php echo get_template_directory_uri()?>/img/galleryList.png" />
                </div>
            </div>
            <div class="copyright">
            	<p>Membership Hotline:86-512-6288-6868</p>
                <p>Copyright: 金鸡湖高尔夫 www.jinjilakegolf.com 2015</p>
            </div>
        </div>
        
    </div>
    
    
    
    <script src="<?php echo get_template_directory_uri()?>/jwplayer/jwplayer.js"></script>  
	<script type="text/javascript">jwplayer.key="tqdHV/3UKPUosuDcvUrzyXtxzgHSFhyvvSCg4g==";</script>
    <script type="text/javascript">
		$(document).ready(function(){  
			function F_JwPlayer(){
			 jwplayer('myplayer').setup({         
				 file: '<?php echo get_template_directory_uri()?>/jwplayer/wer.flv',  
				 stretching: "exactfit",       
				 width: $(window).width(),        
				 height: "100%",
				 autostart: "true",
				 repeat: "true",
				 controls: "false"  
			 });   
			}
			
			F_JwPlayer();
			
			$('.fancybox-buttons').fancybox({
						padding:0,
						margin:0,

						autoSize:true,
						openEffect  : 'elastic',
						closeEffect : 'elastic',
		
						prevEffect : 'fade',
						nextEffect : 'fade',
		
						closeBtn  : false,
		
						helpers : {
							title : {
								type : 'inside'
							},
							buttons	: {}
						},
					});
	 	});
		
		function memberLogin(){
			if($("#account").val()=='马洪霞'){
				window.location.href = 'index2.html';
			}else{
				$(".dHidden").css('display','block');
			}
		}
	</script>
    
    
</body>
</html>
