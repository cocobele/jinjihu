<?php
/**
 * TEMPLATE NAME:课程简介
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=13" class="u1">
                Golf Academy
            </a>
            <a href="?coachtype=golfcoach" class="u2">
            	Golf Coaches
            </a>
            <a href="?course=course" class="cur u3">
            	Golf Lessons
            </a>
        </div>
        <div class="LCont" style="width:733px;">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle alignCenter">Golf Lessons</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<table class="whiteborderTable" cellspacing=0 cellpadding=0>
                        	<?php while ( have_posts() ) : the_post(); ?>
                                    <?php the_content() ?>
                        	<?php endwhile;	?>
                        </table>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/course.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>
