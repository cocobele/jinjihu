<?php

get_header();
?>


<div class="entry">
	
	
	<?php while (have_posts()):the_post()?>
	
	<div class="wrap">
		<?php the_content()?>
	</div>
	
	<?php endwhile;?>
	
</div>


<?php get_footer()?>