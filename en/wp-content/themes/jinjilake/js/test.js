$(document).ready(function(){
	
	setTimeout(function(){	
		F_A_resize();
		$('.LnavBg .cur').attr("href","javascript:void(0);");
	},50);
	
	$('.LnavBg .cur').mousedown(function(){	
		LCont(); 
	});
	
	function F_A_resize(){
		//$('#Logo').css("width",$(window).width()/14.77);
		$('#membersOnly').css("width",$(window).width()/2.44);
		$('#membersOnlyButton').css("width",$(window).width()/8.47);
		$('.LnavBg').css("height",$(window).height()-350);
		$('.LCont').css("height",$(window).height()-350);
		$('.LContMiddle').css("height",$(window).height()-420);
		$('.cont').css("height",$(window).height()-524);
		$('.photogallery').css("height",$(window).height()-350);
		$('.photogallery').css("width",$(window).width()-920);
	}
	
	function leftNav(){
		if($('.LnavBg').css('opacity')==0){
			setTimeout(function(){
				$('.LnavBg').css('opacity',0).animate({  opacity: 1, left: '0' }, 400);
				$('.photogallery').animate({ left: '750' }, 1000);
			},400);
			setTimeout(function(){
				$('.LContHead').animate({  opacity: 1, marginLeft: '533' }, 500);
				$('.LContMiddle').animate({  opacity: 1, marginLeft: '533' }, 700);
				$('.LContFoot').animate({  opacity: 1, marginLeft: '533' }, 900);
				$('.LnavBg .u1').css('opacity',0).animate({ opacity: 1, marginLeft: '0' }, 500);
				$('.LnavBg .u2').css('opacity',0).animate({ opacity: 1, marginLeft: '0' }, 700);
				$('.LnavBg .u3').css('opacity',0).animate({ opacity: 1, marginLeft: '0' }, 900);
				$('.LnavBg .u4').css('opacity',0).animate({ opacity: 1, marginLeft: '0' }, 1100);
				$('.LnavBg .u5').css('opacity',0).animate({ opacity: 1, marginLeft: '0' }, 1300);
				$('.LnavBg .u6').css('opacity',0).animate({ opacity: 1, marginLeft: '0' }, 1500);
			},1000);
			setTimeout(function(){
				$('.close').animate({ opacity: 1}, 400);
				$('.contTitle').animate({ opacity: 1,left:'9%'}, 1000);
				$('.contLine').animate({ opacity: 1}, 1000);
				$('.LCont .cont').animate({ opacity: 1,left:'9%'}, 1000);
				$('.LContLong .cont').animate({ opacity: 1,left:'0'}, 1000);
			},1500);
		}
	}
	
	$(window).resize(function() {
		F_A_resize();
	});
	
	$('.close').mousedown(function(){
		LCont_Close();
	});
	
	$('.moButton').mousedown(function(){
		leftNav();
	});
	
	function LCont(){
		setTimeout(function(){
			$('.LContHead').animate({  opacity: 1, marginLeft: '533' }, 500);
		},0);
		setTimeout(function(){
			$('.LContMiddle').animate({  opacity: 1, marginLeft: '533' }, 500);
		},200);
		setTimeout(function(){
			$('.LContFoot').animate({  opacity: 1, marginLeft: '533' },500);
		},400);
		setTimeout(function(){
			$('.LCont .close').css({'opacity':0,'left':$('.LCont').width()-30}).animate({opacity: 1}, 400);
			$('.LContLong .close').css({'opacity':0,'left':$('.LContLong').width()-30}).animate({opacity: 1}, 400);
			$('.contTitle').animate({ opacity: 1,left:'9%'}, 1000);
			$('.contLine').animate({ opacity: 1}, 1000);
			$('.LCont .cont').animate({ opacity: 1,left:'9%'}, 1000);
			$('.LContLong .cont').animate({ opacity: 1,left:'0'}, 1000);
		},600);
	};	
	
	function LCont_Close(){
		setTimeout(function(){
			//$('.close').animate({ left: 0 }, 600);
			$('.contTitle').animate({ opacity: 0,left:'-100'}, 1000);
			$('.contLine').animate({ opacity: 0}, 1000);
			$('.cont').animate({ opacity: 0,left:'200'}, 1000);
		},0);
		setTimeout(function(){
			$('.LContHead').animate({  opacity: 0, marginLeft: '0' }, 500);
		},500);
		setTimeout(function(){
			$('.LContMiddle').animate({  opacity: 0, marginLeft: '0' }, 500);
		},700);
		setTimeout(function(){
			$('.LContFoot').animate({  opacity: 0, marginLeft: '0' },500);
		},900);
		setTimeout(function(){
			$('.LnavBg').animate({  opacity: 0, left: '-200' },400);
			$('.photogallery').animate({ left: '460' }, 700);
		},1000);
	};	
	
	//.Close
	function Close_(this_,pos,val){	
		setTimeout(function(){	
			$(this_).css("background-position",pos+"px center")
		},val);	
	}
	$(".close").hover(
		function ()
		{	var this_=this;
			Close_(this_,-30,10);
			Close_(this_,-60,15);
			Close_(this_,-90,25);
			Close_(this_,-120,40);
			Close_(this_,-150,60);
			Close_(this_,-180,85);
			Close_(this_,-210,115);
			Close_(this_,-240,150);
			Close_(this_,-270,190);
		},
		function ()
		{	var this_=this;
			Close_(this_,-270,10);
			Close_(this_,-240,15);
			Close_(this_,-210,25);
			Close_(this_,-180,40);
			Close_(this_,-150,60);
			Close_(this_,-120,85);
			Close_(this_,-90,115);
			Close_(this_,-60,150);
			Close_(this_,-30,190);
			Close_(this_,-0,235);
		}
    );
});