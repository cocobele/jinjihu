<?php
/**
 * TEMPLATE NAME:马可波罗西餐厅
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=15" class="u1">
                Pro Shop
            </a>
            <a href="?page_id=50" class="u2">
            	Cigar Bar
            </a>
            <a href="?page_id=52" class="cur u3">
                Marco Polo
            </a>
            <a href="?page_id=54" class="u4">
            	Hu Xuan
            </a>
            <a href="?page_id=56" class="u5">
                VIP Areas
            </a>
            <a href="?page_id=58" class="u6">
            	Conference Room
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle" style="font-size:22px;">Marco Polo Western Restaurant</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<p>The Marco Polo Western Restaurant is on the 2nd floor of the clubhouse. If you'd like a quiet space, you can choose to be indoor; if you'd like to embrace nature, you can enjoy food outside in the balcony, appreciating the green course and breath of sunshine and breeze.</p>
                        <p>Choosing the most natural and fresh food material and the most reasonable way of cooking, our catering team helps you supply nutrients and energy required after playing golf. Also, we continuously introduce new delicacies suitable for different seasons and solar terms to satisfy your taste buds.</p>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/marcoPolo.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>
