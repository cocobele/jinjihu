<?php
/**
 * TEMPLATE NAME:IMG
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=2" class="u1">
                A Private Club
            </a>
            <a href="?page_id=33" class="u2">
            	MBI
            </a>
            <a href="?page_id=31" class="cur u3">
            	IMG
            </a>
           <a href="?honortype=honordetail" class="u4">
            	Honour List
            </a>
            <a href="?namelisttype=namelistdetail" class="u5">
            	Hole in One List
            </a>
            <a href="?champion=champion" class="u6">
            	Club Champions
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle" style="margin-top:16px"><img src="<?php echo get_template_directory_uri()?>/img/IMGLogo1.png" /></div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<p>Established in 1960, IMG is a global sports, fashion and media company with offices all over the world. IMG is the leading golf course management company in Asia and manages a portfolio of high quality clubs including Jinji Lake. IMG's team of experts visit the club on a regular basis providing support and advice in all areas of operations, golf course maintenance and membership sales.</p>
                        <p>Members of Jinji Lake receive access and discounted rates at over 90 elite clubs in the IMG Prestige network. IMG Prestige is a collection of high quality clubs in Asia, Australia, Middle East, Europe and the United States. This is a very valuable member benefit that is available to Jinji Lake members.</p>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/IMG.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
		<div class="foot">
        	<div class="language">
            	<img src="<?php echo get_template_directory_uri()?>/img/sanjiao.png" /> <a href="http://jinjilake.eogor.cn">中文</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">ENGLISH</a>
            </div>
            <div class="copyright">
            	<p>Membership Hotline:86-512-6288-6868</p>
                <p>Copyright: 金鸡湖高尔夫 www.jinjilakegolf.com 2015</p>
            </div>
        </div>
    </div>
    
    <script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/main.js"></script>
</body>
</html>