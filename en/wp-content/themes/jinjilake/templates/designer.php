<?php
/**
 * TEMPLATE NAME:设计师
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=11" class="u1">
                Course
            </a>
            <a href="?page_id=41" class="u2">
            	GEO
            </a>
            <a href="?page_id=9" class="cur u3">
            	Designer
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">Designer</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
						<p>Gary Player – An Uncompromising Perfectionist</p>
                    	<p>Gary Player, renowned in the golf world as one of the "Big Three" and also as the "Black Knight", has designed 3 courses at Suzhou Jinji Lake International Golf Club that will become as legendary and famous as both the ancient city of Suzhou and Gary himself.</p>
						<p>Gary is the most successful international golfer of all time and is as famous as much for his dedication to the principle of excellence as he is for his golfing accomplishments. He has used all these traits to create three masterpieces in China's most booming city - Suzhou.</p>
                        <br/>
                        <p align="right"><img src="" class="signatureImg" style="display:none" /></p>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/designer.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
			setTimeout(function(){
				$('.signatureImg').css('display','block').attr("src","<?php echo get_template_directory_uri()?>/img/signature.gif");
			},3000);
			
			setTimeout(function(){
				$('.signatureImg').attr("src","<?php echo get_template_directory_uri()?>/img/signature.png");
			},5000);
			
		});
		</script>
<?php get_footer(); ?>