<?php
/**
 * TEMPLATE NAME:MBI
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=2" class="u1">
                A Private Club
            </a>
            <a href="?page_id=33" class="cur u2">
            	MBI
            </a>
            <a href="?page_id=31" class="u3">
            	IMG
            </a>
            <a href="?honortype=honordetail" class="u4">
            	Honour List
            </a>
            <a href="?namelisttype=namelistdetail" class="u5">
            	Hole in One List
            </a>
            <a href="?champion=champion" class="u6">
            	Club Champions
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle" style="margin-top:12px;">
                	<p>Member Only private golf club</p>
                </div>
                <div class="contLine" style="margin-top:92px"></div>
                <div class="cont" style="margin-top:106px">
                    <div class="sCont mCustomScrollbar">
                    	<p>Suzhou Jinji Lake International Golf Club is an exclusive, Member Only private golf club, one of the only clubs of its kind in China. Its Membership By Invitation only (MBI) and Membership Interview policies make it unique and very attractive to the invited members.</p>
						<p>MBI (Membership By Invitation) is a policy reserved for the finest private golf clubs throughout the world. One can only become a new member by being invited by a current club member.</p>
						<p>Each applicant is required to participate in a Membership Interview executed by the Membership Admission Committee. The Committee is comprised of a four-person group led by the General Manager, to achieve a mutual understanding and two-way communications between the Club and the member applicants.</p>
                        <div class="scLine"></div>
                        <p class="fontSize16">Membership Hotline: 0512-6288 6868</p>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/MBI.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
        
<?php get_footer(); ?>
