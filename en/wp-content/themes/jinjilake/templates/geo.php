<?php
/**
 * TEMPLATE NAME:GEO
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=11" class="u1">
                Course
            </a>
            <a href="?page_id=41" class="cur u2">
            	GEO
            </a>
            <a href="?page_id=9" class="u3">
            	Designer
            </a>
        </div>
        <div class="LCont" style="width:933px;">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle" style="width:453px;">GEO</div>
                <div class="contLine" style="width:453px;"></div>
                <div class="cont" style="width:453px;">
                    <div class="sCont mCustomScrollbar">
                    	<p>In August 2011, Jinji Lake Golf Club has passed the certification of GEO (European Golf Environment Organization), becoming the first GEO-certified golf club in mainland China; in 2014, the Club again passed GEO's re-certification, taking a solid step toward building an environmental-friendly golf club on the way of sustainable development. </p>
                        <p>To ensure the Club's sustainable development, GEO need to  evaluate on its certified golf club every 3 years, focusing on 6 kinds of fields — water, energy and resource, environmental quality, landscape and ecology, humanistic community, product and supply chain. For a long time, Jinji Lake Golf Club has been making every endeavor to solve problems according to the criteria of GEO; Continuously, the Club will make progress in environmental improvements, not only to provide a healthy course for our golfers, but also to contribute to the society and environment in our own way.
</p>
                    </div>
        		</div>
                <div class="geoCerti mCustomScrollbar">
                	<img src="<?php echo get_template_directory_uri()?>/img/geoCerti.jpg" />
                </div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/geo.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>
