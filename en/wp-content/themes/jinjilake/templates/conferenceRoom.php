<?php
/**
 * TEMPLATE NAME:会议室
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=15" class="u1">
                Pro Shop
            </a>
            <a href="?page_id=50" class="u2">
            	Cigar Bar
            </a>
            <a href="?page_id=52" class="u3">
                Marco Polo
            </a>
            <a href="?page_id=54" class="u4">
            	Hu Xuan
            </a>
            <a href="?page_id=56" class="u5">
                VIP Areas
            </a>
            <a href="?page_id=58" class="cur u6">
            	Conference Room
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">Conference Room</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<p>The 3F conference room is absolutely private, elegant, capacious and bright. The design of the whole room is clear in line, luxurious but modest, steady but not rigid. Equipped with advanced projection equipment, it must be your great choice for the company meeting.</p>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/conferenceRoom.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>