<?php
/**
 * TEMPLATE NAME:贵宾专属区
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=15" class="u1">
                Pro Shop
            </a>
            <a href="?page_id=50" class="u2">
            	Cigar Bar
            </a>
            <a href="?page_id=52" class="u3">
                Marco Polo
            </a>
            <a href="?page_id=54" class="u4">
            	Hu Xuan
            </a>
            <a href="?page_id=56" class="cur u5">
                VIP Areas
            </a>
            <a href="?page_id=58" class="u6">
            	Conference Room
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">VIP Areas</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<p>The VIP area on the 3rd floor in the clubhouse provides you with honorable and luxurious facilities and services. There are three main functional areas: banquet, conference, and locker-room. The capacious and bright banquet box can accommodate up to 20 people to hold a joyous feast, and the whole room is mainly in elegant traditional Chinese style, with huge French window through which you can view all the beautiful lakeside scenery, also with comfortable sofa area in which you can chat and taste tea with friends leisurely.</p>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/vip.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'SW',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'superSlide',
			sequenceMode: 'random',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>
