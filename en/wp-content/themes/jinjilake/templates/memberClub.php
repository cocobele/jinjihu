<?php
/**
 * TEMPLATE NAME:会员制俱乐部
 */
get_header();
?>

        
        <div class="LnavBg">
        	<a href="?page_id=2" class="cur u1">
                A Private Club
            </a>
            <a href="?page_id=33" class="u2">
            	MBI
            </a>
            <a href="?page_id=31" class="u3">
            	IMG
            </a>
            <a href="?honortype=honordetail" class="u4">
            	Honour List
            </a>
            <a href="?namelisttype=namelistdetail" class="u5">
            	Hole in One List
            </a>
            <a href="?champion=champion" class="u6">
            	Club Champions
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">A Private Club</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<p>Located between Jinji Lake and Dushu Lake, signature designed by Gary Player, who renowned in the golf world as one of the "Big Three" and also as the "Black Knight", the three nine holes of different styles — the Forest, the Wetlands and the Links have been combined perfectly into one 27-hole golf course at Suzhou Jinji Lake International Golf Club, which has been regarded as the model of modern city golf.</p>
						<p>As the flagship club fully managed by IMG, Jinji Lake Golf Club provides first-rate services with IMG's world leading management experience. The Club adheres to the MBI(Membership By Invitation) and Membership Interview policies, to achieve a mutual understanding and two-way communications between the club and the members.</p>
                        <p>In 2010, Jinji Lake Golf Club has hosted the 16th Volvo China Open, which is one of the most successful ones in history, and received high praise from worldwide professional golfers and media within the industry by its high standard courses and services.</p> 
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/brief1.jpg','<?php echo get_template_directory_uri()?>/img/brief2.jpg','<?php echo get_template_directory_uri()?>/img/brief.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>
