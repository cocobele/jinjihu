<?php
/**
 * TEMPLATE NAME:湖轩宴会厅
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=15" class="u1">
                Pro Shop
            </a>
            <a href="?page_id=50" class="u2">
            	Cigar Bar
            </a>
            <a href="?page_id=52" class="u3">
                Marco Polo
            </a>
            <a href="?page_id=54" class="cur u4">
            	Hu Xuan
            </a>
            <a href="?page_id=56" class="u5">
                VIP Areas
            </a>
            <a href="?page_id=58" class="u6">
            	Conference Room
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle" style="font-size:22px;">Hu Xuan Chinese Banquet Hall</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<p>Located beside the lake, the HuXuan Chinese Banquet Hall is a great place to appreciate the scenery of Dushu Lake and the golf course. It provides you with customized banquet services with a capacity of up to 150 people. If you want to hold a small type get-together, the DuShu Box and TingLan Box beside the lake will just be your good choices.</p>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/lakeside.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'none',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>