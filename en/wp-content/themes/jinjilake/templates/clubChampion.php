<?php
/**
 * TEMPLATE NAME:俱乐部冠军榜
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=2" class="u1">
                俱乐部简介
            </a>
            <a href="?page_id=33" class="u2">
            	MBI
            </a>
            <a href="?page_id=31" class="u3">
            	IMG
            </a>
            <a href="?page_id=35" class="u4">
            	俱乐部荣誉
            </a>
            <a href="?page_id=37" class="u5">
            	会员一杆进洞扬名榜
            </a>
            <a href="?page_id=39" class="cur u6">
            	俱乐部冠军榜
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle alignCenter" >俱乐部冠军榜</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<ul class="pricing_table">
                            <li class="price_block">
                                <h3>俱乐部男子冠军</h3>
                                <ul class="features">
                                    <li>2015 金哲</li>
                                    <li>2014 虞国荣</li>
                                    <li>2013 虞国荣</li>
                                    <li>2012 田洛彰</li>
                                    <li>2011 田洛彰</li>
                                    <li>2010 田洛彰</li>
                                    <li>2009 田洛彰</li>
                                    <li>2008 田洛彰</li>
                                    <li>2007 张怀竹</li>
                                </ul>
                            </li>
                            <li class="price_block marL3pc">
                                <h3>俱乐部女子冠军</h3>
                                <ul class="features">
                                    <li>2015 伏明敏</li>
                                    <li>2014 黄艳</li>
                                    <li>2013 黄艳</li>
                                    <li>2012 朴喜庆</li>
                                    <li>2011 张毅</li>
                                    <li>2010 张毅</li>
                                    <li>2009 李康心</li>
                                    <li>2008 张毅</li>
                                    <li>2007 李康心</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/clubChampion.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>
