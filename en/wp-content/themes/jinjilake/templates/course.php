<?php
/**
 * TEMPLATE NAME:课程简介
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=13" class="u1">
                学院简介
            </a>
            <a href="?page_id=43" class="u2">
            	教练简介
            </a>
            <a href="?page_id=45" class="cur u3">
            	课程简介
            </a>
        </div>
        <div class="LCont" style="width:733px;">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle alignCenter">高尔夫课程介绍</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<table class="whiteborderTable" cellspacing=0 cellpadding=0>
                        	<tr>
                            	<td class="tdal">
                                	团体教学（含四位学员）
                                </td>
                                <td>
                                	2小时
                                </td>
                                <td>
                                	4:1
                                </td>
                                <td>
                                	1200
                                </td>
                                <td>
                                	
                                </td>
                            </tr>
                            <tr>
                            	<td class="tdal">
                                1小时私人教学
                                </td>
                                <td>
                                1小时
                                </td>
                                <td>
                                1:1
                                </td>
                                <td>
                                500
                                </td>
                                <td>
                                
                                </td>
                            </tr>
                            <tr>
                            	<td class="tdal">
                                1小时私人教学（嘉宾）
                                </td>
                                <td>
                                1小时
                                </td>
                                <td>
                                1:1
                                </td>
                                <td>
                                550
                                </td>
                                <td>
                                
                                </td>
                            </tr>
                            <tr>
                            	<td class="tdal">
                                私人教学套餐
                                </td>
                                <td>
                                11小时
                                </td>
                                <td>
                                1:1
                                </td>
                                <td>
                                4000
                                </td>
                                <td class="tdal">
                                （9）小时私人教学+（9）洞场地打球教学
                                </td>
                            </tr>
                            <tr>
                            	<td class="tdal">
                                私人教学套餐（嘉宾）
                                </td>
                                <td>
                                11小时
                                </td>
                                <td>
                                1:1
                                </td>
                                <td>
                                5000
                                </td>
                                <td class="tdal">
                                （9）小时私人教学+（9）洞场地打球教学
                                </td>
                            </tr>
                            <tr>
                            	<td class="tdal">
                                场地打球教学
                                </td>
                                <td>
                                2小时
                                </td>
                                <td>
                                1:1
                                </td>
                                <td>
                                800
                                </td>
                                <td class="tdal">
                                2小时 场地教学
                                </td>
                            </tr>
                            <tr>
                            	<td class="tdal">
                                场地打球教学（嘉宾）
                                </td>
                                <td>
                                2小时
                                </td>
                                <td>
                                1:1
                                </td>
                                <td>
                                800
                                </td>
                                <td class="tdal">
                                2小时 场地教学
                                </td>
                            </tr>
                            <tr>
                            	<td class="tdal">
                                全天私人教学
                                </td>
                                <td>
                                7小时
                                </td>
                                <td>
                                1:1
                                </td>
                                <td>
                                3000
                                </td>
                                <td class="tdal">
                                9am-5pm
                                </td>
                            </tr>
                            <tr>
                            	<td class="tdal">
                                全天私人教学（嘉宾）
                                </td>
                                <td>
                                7小时
                                </td>
                                <td>
                                1:1
                                </td>
                                <td>
                                3500
                                </td>
                                <td class="tdal">
                                9am-5pm
                                </td>
                            </tr>
                        </table>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/course.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>
