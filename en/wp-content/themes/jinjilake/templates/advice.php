<?php
/**
 * TEMPLATE NAME:意见和建议
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="advice.html" class="cur u1">
                Comments&Advice
            </a>
        </div>
        <div class="LCont" style="width:733px;">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">Comments and Advice</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar tempPic advicesont">
                        <?php echo do_shortcode('[contact-form-7 id="151" title="意见与建议"]')?>
                        <div class="clearFloat"></div>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/advice.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>