<?php
/**
 * TEMPLATE NAME:最新动态
 */
get_header();
?>       
        
        <div class="LnavBg">
        	<a href="?page_id=17" class="cur u1">
                俱乐部动态
            </a>
            <a href="?page_id=60" class="u2">
            	电子会刊
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">俱乐部动态</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar newsList">
                    	<ul>
                        	俱乐部冠军赛暨建屋康帝庄园邀请赛圆满收杆 (2015-10-15)
                        </ul>
                        <ul>
                        	2015太和•鸿艺会杯夏季灯光高尔夫锦标赛完美落幕 (2015-10-15)
                        </ul>
                        <ul>
                        	夏季会员生日会——缤纷の金鸡湖高尔夫之夜(2015-07-15)
                        </ul>
                        <ul>
                        	2015会员嘉宾配对赛完美落幕(2015-05-31)
                        </ul>
                        <ul>
                        	开年大战，春季杯完美收杆！(2015-4-12)
                        </ul>
                        <ul>
                        	春季生日会，偷得浮生半日闲
                        </ul>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/news.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
        
<?php get_footer(); ?>