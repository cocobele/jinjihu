<?php
/**
 * TEMPLATE NAME:会所
 */
get_header();
?>       
        <div class="LnavBg">
        	<a href="?page_id=15" class="cur u1">
                Pro Shop
            </a>
            <a href="?page_id=50" class="u2">
            	Cigar Bar
            </a>
            <a href="?page_id=52" class="u3">
                Marco Polo
            </a>
            <a href="?page_id=54" class="u4">
            	Hu Xuan
            </a>
            <a href="?page_id=56" class="u5">
                VIP Areas
            </a>
            <a href="?page_id=58" class="u6">
            	Conference Room
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">Pro Shop</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<p>The Pro Shop at the first floor of the clubhouse is equipped with various golf products of well-known brands or with the logo of IMG or Jinji Lake Golf Club. FootJoy,Titleist, Crest Link, Oakley, Pro Simon,  Number, Under Armour, Helix, Honma, Abacus, Cleveland, Srixon……you must can find your suitable one among so many brands.</p>
						<p>To provide you with more benefits, the Pro Shop will give discounts to members according to different conditions. If you have any questions or comments, welcome to inquiry our staff at the Pro Shop.</p>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/exclusiveShop.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
        
<?php get_footer(); ?>