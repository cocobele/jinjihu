<?php
/**
 * TEMPLATE NAME:俱乐部荣誉
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=2" class="u1">
                俱乐部简介
            </a>
            <a href="?page_id=33" class="u2">
            	MBI
            </a>
            <a href="?page_id=31" class="u3">
            	IMG
            </a>
            <a href="?page_id=35" class="cur u4">
            	俱乐部荣誉
            </a>
            <a href="?page_id=37" class="u5">
            	会员一杆进洞扬名榜
            </a>
            <a href="?page_id=39" class="u6">
            	俱乐部冠军榜
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle alignCenter">俱乐部荣誉</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<ul class="threePart">
                        	<li class="alignCenter">
                            	<div class="partTop">
                                	<a href="<?php echo get_template_directory_uri()?>/img/honor1.jpg" class="fancybox" data-fancybox-group="gallery" title="金牌会员服务">
                            			<img src="<?php echo get_template_directory_uri()?>/img/honor1.jpg" />
                                    </a>
                                </div>
                                <div class="partBottom">
                                    金牌会员服务<br/>
                                    颁奖单位：泛高尔夫网
                                </div>
                            </li>
                            <li class="alignCenter">
                            	<div class="partTop">
                                	<a href="<?php echo get_template_directory_uri()?>/img/honor2.jpg" class="fancybox" data-fancybox-group="gallery" title="2014十大锦标赛球场网">
                            			<img src="<?php echo get_template_directory_uri()?>/img/honor2_thumb.jpg" />
                                    </a>
                                </div>
                                <div class="partBottom">
                                    十大锦标赛球场<br/>
                                    《世界高尔夫》
                                </div>
                            </li>
                            <li class="alignCenter">
                            	<div class="partTop">
                                	<a href="<?php echo get_template_directory_uri()?>/img/honor3.jpg" class="fancybox" data-fancybox-group="gallery" title="2011中国十佳高尔夫俱乐部网">
                            			<img src="<?php echo get_template_directory_uri()?>/img/honor3_thumb.jpg" />
                                    </a>
                                </div>
                                <div class="partBottom">
                                	中国十佳高尔夫俱乐部<br/>
                                    《高尔夫大师》
                                </div>                                
                            </li><div class="clearFloat"></div>
                        </ul>
                        
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/honor.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
			$('.fancybox').fancybox();
		});
		</script>
       <?php get_footer(); ?>

