<?php
/**
 * TEMPLATE NAME:高尔夫学院
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=13" class="cur u1">
                Golf Academy
            </a>
            <a href="?coachtype=golfcoach" class="u2">
            	Golf Coaches
            </a>
            <a href="?course=course" class="u3">
            	Golf Lessons
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">Golf Academy</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<p>The Jinji Lake Golf Academy offers excellent opportunities to improve one's skills through the expertise of the Club's internationally recognized PGA staff and state of the art teaching facilities.</p>
                        <p>Whether you want to learn golf from the beginning or want to improve in a specific area, Individual Lessons and Group Lessons can provide improvement for both beginners and experienced players in any aspect of the game of golf. </p>
                        <p>The Jinji Lake Golf Academy features a specially designed video analysis suite and uses the latest V1 computer software to offer the highest standard of swing analysis available in golf instruction.</p>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/schoolBrief.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>