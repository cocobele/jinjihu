<?php
/**
 * TEMPLATE NAME:球场
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=11" class="cur u1">
                Course
            </a>
            <a href="?page_id=41" class="u2">
            	GEO
            </a>
            <a href="?page_id=9" class="u3">
            	Designer
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle">Course</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<p>Located between Jinji Lake and Dushu Lake, three nine holes of different styles — the Forest, the Wetlands and the Links have been combined perfectly into one 27-hole golf course at Jinji Lake Golf Club, including three halfway houses blending peacefully into the surroundings of their respective courses; the courses also feature ten different bridges, styled from the cultures of Suzhou and ancient China.</p>
                        <div class="threePart">
                        	<li style="height:150px">
                            	<div class="partTop" style="height:110px">
                            		<img src="<?php echo get_template_directory_uri()?>/img/courtThumb.jpg" width="100%" />
                                </div>
                                <div class="partBottom" style="height:30px;text-align:center;">
                            		Forest
                                </div>
                            </li>
                            <li style="height:150px">
                            	<div class="partTop" style="height:110px">
                            		<img src="<?php echo get_template_directory_uri()?>/img/wetlandThumb.jpg" width="100%" />
                                </div>
                                <div class="partBottom" style="height:30px;text-align:center;">
                            		Wetlands
                                </div>
                            </li>
                            <li style="height:150px">
                            	<div class="partTop" style="height:110px">
                            		<img src="<?php echo get_template_directory_uri()?>/img/beachThumb.jpg" width="100%" />
                                </div>
                                <div class="partBottom" style="height:30px;text-align:center;">
                            		Links
                                </div>
                            </li>
                            <div class="clearFloat"></div>
                        </div>
						<p>The Forest Course   is set amidst tall and elegant trees, and with its high-tech floodlights, offers hours of golfing enjoyment, day and night.</p>
                        <p>The Wetlands Course meanders pleasantly around lakes, rivers and ponds.</p>
                        <p>The Links Course   provides the sternest challenge with its unique Gary Player designed "pot bunkers" and fairways lined by tall fescue grasses which makes it reminiscent of playing golf in Scotland.</p>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/court.jpg','<?php echo get_template_directory_uri()?>/img/wetland.jpg','<?php echo get_template_directory_uri()?>/img/beach.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>
