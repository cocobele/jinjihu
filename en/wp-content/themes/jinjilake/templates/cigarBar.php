<?php
/**
 * TEMPLATE NAME:雪茄吧
 */
get_header();
?>
        
        <div class="LnavBg">
        	<a href="?page_id=15" class="u1">
                Pro Shop
            </a>
            <a href="?page_id=50" class="cur u2">
            	Cigar Bar
            </a>
            <a href="?page_id=52" class="u3">
                Marco Polo
            </a>
            <a href="?page_id=54" class="u4">
            	Hu Xuan
            </a>
            <a href="?page_id=56" class="u5">
                VIP Areas
            </a>
            <a href="?page_id=58" class="u6">
            	Conference Room
            </a>
        </div>
        <div class="LCont">
        	<div class="LContHead">
            	<a class="close"></a>
            </div>
            <div class="LContMiddle">
            	<div class="contTitle" style="font-size:22px;">Cigar Bar – Gary Player's Lounge</div>
                <div class="contLine"></div>
                <div class="cont">
                    <div class="sCont mCustomScrollbar">
                    	<p>
                        	<div class="threePic">
                            	<ul>
                                	<li>
                                    	<img src="<?php echo get_template_directory_uri()?>/img/cigar1.jpg" />
                                    </li>
                                    <li>
                                    	<img src="<?php echo get_template_directory_uri()?>/img/cigar2.jpg" />
                                    </li>
                                    <li>
                                    	<img src="<?php echo get_template_directory_uri()?>/img/cigar3.jpg" />
                                    </li>
                                </ul>
                                <div class="clearFloat"></div>
                            </div>
                            
                        </p>
                    	<p>The Cigar Bar is named as Gary Player's Lounge after the course designer Mr. Gary Player, renowned in the golf world as one of the "Big Three" and also as the "Black Knight". There are a series of collections as symbol of his honor in this private and quiet lounge. You can enjoy your time leisurely after golfing, sharing playing experience, or discussing business with your bosom friends or partners.</p>
                    </div>
        		</div>
            </div>
            <div class="LContFoot"></div>
        </div>
        <script type=""text/javascript"">
		$(document).ready(function(){
		  $('BODY').bgStretcher({
			images: ['<?php echo get_template_directory_uri()?>/img/cigarBar.jpg'],
			imageWidth: 1920,
			imageHeight: 900,
			slideDirection: 'N',
			nextSlideDelay:6000,
			slideShowSpeed: 1000,
			transitionEffect: 'fade',
			sequenceMode: 'normal',
			buttonPrev: '#prev',
			buttonNext: '#next',
			pagination: '#nav',
			anchoring: 'left center',
			anchoringImg: 'left center'
			});
			
		});
		</script>
<?php get_footer(); ?>